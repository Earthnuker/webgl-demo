# Myfirstdemo

A 4k WebGL demo

## Prerequisites

- [Shader Minifier](https://github.com/laurentlb/Shader_Minifier/releases) save as `shader_minifier.exe` next to `build.py`
- [Google Closure Compiler](https://developers.google.com/closure/compiler/) save as `cc.jar` next to `build.py`
- [Python3](http://python.org/)
- [Numpy](http://numpy.org/)
- [Imageio](https://imageio.github.io/)
- GCC

## Files

- `config.json`: Configuration file for `build.py`
- `build.py`: assemble final `.html` file
