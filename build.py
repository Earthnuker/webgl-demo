import subprocess as SP
import json
from tempfile import TemporaryDirectory
import os
import zlib
import struct
import math
import numpy as np
from imageio import imwrite
from datetime import datetime


def prepare_shaders(shaders, *args):
    prec = "precision lowp float;"
    code = ""
    sizes = {}
    for k, v in shaders.items():
        with open(v) as sf:
            print("[*] Reading", v)
            code += "#ifdef {}\n{}\n#endif\n".format(k, sf.read())
            sizes[k] = sf.tell() + len(prec)
    orig_size = len(code)
    while "\n\n" in code:
        code = code.replace("\n\n", "\n")
    with TemporaryDirectory() as folder:
        path = os.path.join(folder, "shader.glsl")
        path_min = os.path.join(folder, "shader.min.glsl")
        with open(path, "w") as of:
            of.write(code)
        print("[*] Minifying", path)
        cmd = [
            "shader_minifier.exe",
            "--preserve-externals",
            "--smoothstep",
            "--format",
            "none",
            "-o",
            path_min,
            path,
        ]
        if os.name == "posix":
            cmd = ["mono"] + cmd
        SP.run(cmd)
        for k in shaders:
            pipe = SP.Popen(["cpp", "-D", k, path_min], stdout=SP.PIPE)
            shader_code = str(pipe.stdout.read(), "utf-8")
            pipe.wait()
            shader_code = (
                "\n".join(
                    [l for l in shader_code.splitlines() if not l.startswith("#")]
                )
                .strip()
                .replace("\r", "")
            )
            while "\\n\\n" in shader_code:
                shader_code = shader_code.replace("\\n\\n", "\\n")
            while "\n\n" in shader_code:
                shader_code = shader_code.replace("\n\n", "\n")
            shader_code = shader_code.replace("\n", "\\n")
            shaders[k] = prec + shader_code
            print(
                "[*] Shader [{}] size: {} -> {} ({:.02%})".format(
                    k, sizes[k], len(shaders[k]), (len(shaders[k]) / sizes[k])
                )
            )
    return orig_size, shaders


def process_js(file, *args):
    print("[*] Minifying", file)
    orig_size = os.stat(file).st_size
    pipe = SP.Popen(
        ["java", "-jar", "cc.jar", "-O", "ADVANCED", *args, file], stdout=SP.PIPE
    )
    ret = str(pipe.stdout.read(), "utf-8").strip()
    if pipe.wait() != 0:
        exit("Minification Failed!")
    print(
        "[*] JS size: {} -> {} ({:.02%})".format(
            orig_size, len(ret), (len(ret) / orig_size)
        )
    )
    return orig_size, ret


def opt_png(file, *args, **kwargs):
    orig_size = len(open(file, "rb").read())
    SP.run(["optipng", "-o7", "-zm1-9", "-quiet", file])
    with open(file, "rb") as inf:
        opt_data = inf.read()
    return orig_size, opt_data


def build_png(data, boot, big=False):
    data = b"\x00" + data
    print("[*] Optimizing PNG ({} byte of data)".format(len(data)))
    minsize = float("inf")
    with TemporaryDirectory() as folder:
        w = len(data)
        a_data = np.array(list(data), dtype=np.uint8)
        outfile = os.path.join(folder, "out.png")
        a_data.resize((w,), refcheck=False)
        img = a_data.reshape((1, w, 1))
        imwrite(outfile, img, format="png")
        orig_size, png_data = opt_png(outfile)
        if len(png_data) < minsize:
            minsize = len(png_data)
            best = {"size": len(png_data), "orig_size": orig_size, "data": png_data}
    print(
        "[*] PNG Size: {orig_size} -> {size} ({:.02%})".format(
            (best["size"] / best["orig_size"]), **best
        )
    )
    png_data = best["data"]
    boot = boot.replace("__WIDTH__", str(len(data)))
    boot = boot.replace("__HEIGHT__", str(1))
    boot = boot.replace("\n", ";")
    while "  " in boot:
        boot = boot.replace("  ", " ")
    boot = b'<canvas id=c><img onload="' + bytes(boot, "utf-8") + b'" src=#>'
    if png_data.rindex(b"<") > png_data.rindex(b">"):
        boot = b">" + boot
    if big:
        return len(boot), png_data + boot
    iend = png_data.rindex(b"IEND") - 4
    png_data = png_data[:iend] + int.to_bytes(len(boot) - 4, 4, "little") + boot
    return len(boot), png_data


cfg = json.load(open("config.json"))
t_start = datetime.today()
js_size, js = process_js("src/main.js")
boot = cfg["BOOT"]
shader_size, shaders = prepare_shaders(cfg["SHADER"])
for k, v in shaders.items():
    js = js.replace("__{}_SHADER__".format(k), v)
print("[*] JS+Shaders size: {} byte".format(len(js)))
html = (
    "<style>body,canvas{width:100%;height:100%;margin:0}</style><canvas id=c><script>"
    + js
    + "</script>"
)
html_size = len(html) - len(js)
print("[*] Raw HTML size: {} byte".format(len(html)))
with open("raw.html", "w") as of:
    of.write(html)

with open("index.html", "wb") as of:
    boot_size, data = build_png(bytes(html, "utf-8"), boot)
    of.write(data)
    orig_size = html_size + boot_size + shader_size + js_size
    new_size = of.tell()
    print(
        "[*] Final size: {} -> {} byte ({:.02%}) ({:+} byte)".format(
            orig_size, new_size, (new_size / orig_size), 2048 - new_size
        )
    )
print("[*] Done in {}".format(datetime.today() - t_start))
print("=" * 20)

