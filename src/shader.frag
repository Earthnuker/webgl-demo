uniform float t;
uniform vec2 s;
const float EPSILON = 1. / 100.0;
const vec3 fogcol = vec3(0.);

vec3 hsv2rgb(vec3 c) {
  vec4 K = vec4(1., 2. / 3., 1. / 3., 3.);
  vec3 p = abs(fract(c.xxx + K.xyz) * 6. - K.www);
  return c.z * mix(K.xxx, clamp(p - K.xxx, 0., 1.), c.y);
}
float hash(float n) { return fract(sin(n) * 753.5453123); }
float noise(vec3 x) {
  vec3 p = floor(x);
  vec3 f = fract(x);
  f = f * f * (3. - 2. * f);

  float n = p.x + p.y * 157. + 113. * p.z;
  return mix(mix(mix(hash(n + 0.), hash(n + 1.), f.x),
                 mix(hash(n + 157.), hash(n + 158.), f.x), f.y),
             mix(mix(hash(n + 113.), hash(n + 114.), f.x),
                 mix(hash(n + 270.), hash(n + 271.), f.x), f.y),
             f.z);
}

vec2 rotate(vec2 pos, float angle) {
  float c = cos(angle);
  float s = sin(angle);
  return mat2(c, s, -s, c) * pos;
}

float smin(float a, float b, float k) {
  float h = clamp(0.5 + 0.5 * (b - a) / k, 0., 1.);
  return mix(b, a, h) - k * h * (1. - h);
}
float sphere(vec3 pos, float radius) { return length(pos) - radius; }

float round_box(vec3 pos, float size, float radius) {
  return length(max(abs(pos) - size, 0.)) - radius;
}

float distort(vec3 p, float n) { return (1. + (sin(t * n)) / 2.) * 0.5; }
float blob(vec3 p, float o, float a) {
  float v = (6. - a);
  return sphere(p - vec3(sin(v + t * 2.5) + o, sin(a + t * a), cos(v + t * v)),
                0.75);
}
float distfunc(vec3 p) {
  // Move World
  p.xy = rotate(p.xy, sin(0.1 * t + p.z * p.z * 0.001) * p.x * 0.01);
  p.xz = rotate(p.xz, sin(0.3 * t + p.y * p.y * 0.001) * p.x * 0.01);
  p.zy = rotate(p.zy, sin(0.5 * t + p.x * p.x * 0.001) * p.x * 0.01);
  float o = sin(t / 30.) * 250.;
  p.x += o;

  // Metaballs
  float spheres = smin(
      smin(smin(smin(blob(p, o, 1.), blob(p, o, 2.), 1.), blob(p, o, 3.), 1.),
           blob(p, o, 5.), 1.),
      blob(p, o, 7.), 1.);

  p.zy = rotate(p.zy, 20. * sin(p.x * 0.001));

  float mx = distort(p, 0.005);
  float mz = distort(p, 0.07);
  float my = distort(p, 0.03);
  p.x *= mx;
  p.z *= mz;
  p.y *= my;
  float n = noise(p * (0.5 + abs(0.1 * sin(t * 0.1)))) * abs(sin(t * 0.1));
  p = mod(p, vec3(15)) - 0.5 * vec3(15);
  float bx = round_box(p - vec3(p.x, 0, 0), 1., 0.5 + n);
  float by = round_box(p - vec3(0, p.y, 0), 1., 0.5 + n);
  float bz = round_box(p - vec3(0, 0, p.z), 1., 0.5 + n);
  return min(smin(bx, smin(by, bz, 0.5), 0.5), spheres);
}

vec3 trace(vec3 ray, vec3 dir, vec3 light) {
  vec3 col = fogcol;
  vec3 normal;
  float dist;
  for (int rd = 0; rd <= 1; rd++) {
    for (int i = 0; i < 1000; i++) {
      dist = distfunc(ray); // Evalulate the distance at the current point
      if (dist < EPSILON) {
        vec2 eps = vec2(0., EPSILON);
        normal =
            normalize(vec3(distfunc(ray + eps.yxx) - distfunc(ray - eps.yxx),
                           distfunc(ray + eps.xyx) - distfunc(ray - eps.xyx),
                           distfunc(ray + eps.xxy) - distfunc(ray - eps.xxy)));
        float fog = 1. - exp(-length(ray) * 0.04);
        float diffuse = max(0., dot(normalize(light - ray), normal));
        vec3 diff_col =
            mix(hsv2rgb(vec3(fract(t * 0.1 + float(i) / 100.0), 1., 1.)),
                vec3(1.), 3. * pow(fog, 6.));
        vec3 final_color = mix(
            (diff_col * (diffuse + (10. * pow(diffuse, 100.)))), fogcol, fog);
        col = mix(col, final_color, pow(.5, float(rd + int(rd > 0))));
        break;
      }
      ray += dist * dir * 0.5; // Advance the point forwards in the ray
                               // direction by the distance
    }
    dir = reflect(dir, normal);
    ray += dir * EPSILON;
  }
  return col * 1.8;
}

void main() {
  vec2 pc = gl_FragCoord.xy;
  vec3 cameraOrigin = vec3(3.0, 0.0, 0.0);
  cameraOrigin.xy = rotate(cameraOrigin.xy, t * 0.7);
  cameraOrigin.xz = rotate(cameraOrigin.xz, t * 0.5);
  vec3 cameraDir = normalize(-cameraOrigin);
  vec3 cameraRight = normalize(cross(vec3(0., 0., 1.), cameraOrigin));
  vec2 screenPos =
      -1.0 + 2.0 * pc.xy / s.xy; // screenPos can range from -1 to 1
  vec2 o_screenPos = screenPos;
  screenPos.x *= s.x / s.y; // Correct aspect ratio
  screenPos *= 4.0;         // FOV
  vec3 forward =
      normalize(cameraRight * screenPos.x +
                cross(cameraDir, cameraRight) * screenPos.y + cameraDir);
  vec3 res = trace(cameraOrigin, forward, cameraOrigin);

  // scanlines + vignette
  res *= (mod(pc.y + vec3(0.0, 1.0, 2.0), 3.0)) * (1.0 - length(o_screenPos));

  gl_FragColor = vec4(res, 1.0);
}