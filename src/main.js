var gl;
var canvas;
var verts;
var program;
var frag;
var vert;
var positionLocation;
var loc = {}

function resize(canvas) {
    var realToCSSPixels = window.devicePixelRatio;

    // Lookup the size the browser is displaying the canvas in CSS pixels
    // and compute a size needed to make our drawingbuffer match it in
    // device pixels.
    var displayWidth = Math.floor(canvas.clientWidth * realToCSSPixels);
    var displayHeight = Math.floor(canvas.clientHeight * realToCSSPixels);

    // Check if the canvas is not the same size.
    if (canvas.width !== displayWidth ||
        canvas.height !== displayHeight) {

        // Make the canvas the same size
        canvas.width = displayWidth;
        canvas.height = displayHeight;
    }
}

function render(t, program) {
    window.requestAnimationFrame((t) => {
        render(t, program)
    }, canvas);
    resize(canvas);
    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.clear(gl.COLOR_BUFFER_BIT);

    gl.enableVertexAttribArray(positionLocation = gl.getAttribLocation(program, "p"));
    gl.vertexAttribPointer(positionLocation, 2, gl.FLOAT, 0, 0, 0);
    gl.uniform1f(loc.t, (t / 1000.0));
    gl.uniform2f(loc.s, gl.drawingBufferWidth, gl.drawingBufferHeight);
    gl.drawArrays(gl.TRIANGLES, 0, 3);
}

canvas = document.getElementById('c');
gl = canvas.getContext('webgl');
canvas.width = canvas.clientWidth;
canvas.height = canvas.clientHeight;

gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);
gl.bindBuffer(gl.ARRAY_BUFFER, gl.createBuffer());
gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([-1.0, -1.0, 3.0, -1.0, -1.0, 3.0]), gl.STATIC_DRAW);
gl.shaderSource(vert = gl.createShader(gl.VERTEX_SHADER), "__VERTEX_SHADER__");
gl.compileShader(vert);

gl.shaderSource(frag = gl.createShader(gl.FRAGMENT_SHADER), "__FRAGMENT_SHADER__");
gl.compileShader(frag);

program = gl.createProgram();
gl.attachShader(program, vert);
gl.attachShader(program, frag);
gl.linkProgram(program);

/*
// DEBUG
if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
    var info = gl.getProgramInfoLog(program);
    var v_info = gl.getShaderInfoLog(vert);
    var f_info = gl.getShaderInfoLog(frag);
    throw new Error('Could not compile WebGL program. \n\nINFO:\n' + info + '\n\nVERT:\n' + v_info + '\n\nFRAG:\n' + f_info);
}
*/
gl.useProgram(program);

loc.s = gl.getUniformLocation(program, 's');
loc.t = gl.getUniformLocation(program, 't');

window.requestAnimationFrame((t) => {
    render(t, program)
}, canvas);